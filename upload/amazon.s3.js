const aws = require('aws-sdk');
const multer = require('multer');
const config=require('../config/config');
const multerS3 = require('multer-s3');

// const s3 = new aws.S3({
//   accessKeyId: 'AKIAIMM7AUUDS5VRMRMA',
//   secretAccessKey: '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',
//   region: 'ap-south-1'
// });

aws.config.update({
  AccessKeyID: config.AccessKeyID,
  SecretAccessKey: config.SecretAccessKey,
  region: 'us-east-1'
});

const s3 = new aws.S3();

const fileFilter = (req, file, cb) => {
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
    cb(null, true);
  } else {
    cb(new Error('Invalid file type, only JPEG and PNG is allowed!'), false);
  }
}

const upload = multer({
  fileFilter,
  storage: multerS3({
    acl: 'public-read',
    s3,
    bucket: 'bwm-ng-dev',
    metadata: function (req, file, cb) {
      cb(null, {fieldName: 'TESTING_METADATA'});
    },
    key: function (req, file, cb) {
      cb(null, Date.now().toString())
    }
  })
});




  
  module.exports = upload;