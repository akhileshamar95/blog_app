const express = require('express');
const router = express.Router();


const checkAuth = require('../../moddleware/Auth');


/************************************
 * Blog CONTROLLERS
 ***********************************/
 const routeBlog=require('../controllers/Blog.controllers');
 
/**************************************************************
 * @ROUTE       - /admin/blog/create-Blog
 * @METHOD      - POST
 * @DESC        - TO CREATE A Blog

***************************************************************/
router.post("/create-Blog",checkAuth, routeBlog.BlogControllersPOST );


/**************************************************************
 * @ROUTE       - /admin/blog/get-Blog
 * @METHOD      - GET
 * @DESC        - TO READ A Blog

***************************************************************/
router.get("/get-Blog",routeBlog.BlogControllersGET );


/**************************************************************
 * @ROUTE       - /admin/blog/update:BlogId
 * @METHOD      - PUT
 * @DESC        - UPDATE A Blog

***************************************************************/
router.patch("/update:BlogId", checkAuth,routeBlog.BlogControllersPATCH);


/**************************************************************
 * @ROUTE       - /admin/blog/delete:BlogId
 * @DESC        - UPDATE A Blog

***************************************************************/
router.delete("/delete:BlogId", checkAuth, routeBlog.BlogControllersDELETE);



/***************************************************************
 * Users middleware 
 ***************************************************************/
module.exports = router;
  





