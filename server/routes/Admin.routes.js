
const express = require('express');
const router = express.Router();



const checkAuth = require('../../moddleware/Auth');


/************************************
 * ADMIN CONTROLLERS
 ***********************************/
const AdminloginControllers= require('../controllers/Admin.controller');

/**************************************************************
 * @ROUTE       - /admin/signup
 * @METHOD      - POST
***************************************************************/
router.post('/signup', AdminloginControllers.usersignuppost);


/**************************************************************
 * @ROUTE       - /admin/login
 * @METHOD      - POST
***************************************************************/
router.post("/login", checkAuth,AdminloginControllers.userloginpost);


/**************************************************************
 * @ROUTE       -/admin/User/signup
 * @METHOD      - POST
***************************************************************/
router.post("/User/signup",checkAuth, AdminloginControllers.Usersignuppost);


/**************************************************************
 * @ROUTE       -/admin/User/get-users
 * @METHOD      - GET
***************************************************************/
router.get("/User/get-users",checkAuth, AdminloginControllers.UserControllersGET);



/***************************************************************
 * Admin middleware 
 ***************************************************************/
module.exports = router;
