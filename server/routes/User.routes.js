
const express = require('express');
const router = express.Router();



const checkAuth = require('../../moddleware/Auth');


/************************************
 * USER CONTROLLERS
 ***********************************/
const UserloginControllers= require('../controllers/User.controllers');




/**************************************************************
 * @ROUTE       -/admin/User/signup
 * @METHOD      - POST
***************************************************************/
// router.post("/signup", checkAuth,UserloginControllers.Usersignuppost);


/**************************************************************
 * @ROUTE       - /admin/User/login
 * @METHOD      - POST
***************************************************************/
router.post("/login", checkAuth,UserloginControllers.UserLoginppost);



/***************************************************************
 * USER middleware 
 ***************************************************************/
module.exports = router;
