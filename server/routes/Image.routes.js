const express = require('express');
const router = express.Router();
const checkAuth = require('../../moddleware/Auth');
// IMAGE UPLOADER
const upload = require('../../upload/amazon.s3');
/************ ROUTER  **************/
const singleUpload = upload.single('image');


router.post('/image-upload', checkAuth, function(req, res) {
  singleUpload(req, res, function(err) {
    if (err) {
      return res.status(422).send({errors: [{title: 'Image Upload Error', detail: err.message}]});
    }

    return res.json({'imageUrl': req.file.location});
  });
});





// const Images = require('../models/Image.model');
// const singleUpload = upload.single('image');
// // @ ROUTE POST /admin/images/upload-images
// // @ DESC - UPLOAD the Images
// // @ ACCESS - CSRF PROTECTION

// router.post('/upload',singleUpload , async ( req, res, next ) => {
//   const newImage = new Images({
//       image_url: req.file.location
//   });
//   let save  = await newImage.save();
//   if( save ){
//       return  res.json({ image_URL : req.file.location });
//   }
// })

module.exports = router;