const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const imagesSchema = new Schema({
    image:{
        type : String,
        required : true
    }
});

module.exports = mongoose.model('Images', imagesSchema);