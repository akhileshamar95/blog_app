
const config = require('config');
const jwt = require('jsonwebtoken');
const Joi = require('joi');
const mongoose = require('mongoose');

const productSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
   

    Headline_of_Blogs:{type: String, required: true},
    Tag: { type: String, required: true},
    Content:{ type: String , required: true },
    Start_date: { type : Date },
    Publication_Date: {type : Date},
    Author_Name: { type: String, required: true},
    Date: { type: Date, default: Date.now },

    // image:{ type: Number},
});

module.exports = mongoose.model('BLOGPOST', productSchema);